Callbacks = {

    error: function (reason) {
        window.SOCKET_ERROR_REASON = reason;
    },

    /* fired when socket connection completes */
    connect: function() {
        HAS_CONNECTED_BEFORE = true;
        SOCKETIO_CONNECT_ERROR_COUNT = 0;
        $("#socketio-connect-error").remove();
        socket.emit("joinChannel", {
            name: CHANNEL.name
        });

        if (CHANNEL.opts.password) {
            socket.emit("channelPassword", CHANNEL.opts.password);
        }

        if (CLIENT.name && CLIENT.guest) {
            socket.emit("login", {
                name: CLIENT.name
            });
        }

        $("<div/>").addClass("server-msg-reconnect")
            .text("Connected")
            .appendTo($("#messagebuffer"));
        scrollChat();
        stopQueueSpinner(null);
    },

    disconnect: function() {
        if(KICKED)
            return;
        $("<div/>")
            .addClass("server-msg-disconnect")
            .text("Disconnected from server.")
            .appendTo($("#messagebuffer"));
        scrollChat();
    }
};

function ioServerConnect(socketConfig) {
    if (socketConfig.error) {
        makeAlert("Error", "Socket.io configuration returned error: " +
                socketConfig.error, "alert-danger")
            .appendTo($("#announcements"));
        return;
    }

    var servers;
    if (socketConfig.alt && socketConfig.alt.length > 0 &&
            localStorage.useAltServer === "true") {
        servers = socketConfig.alt;
        console.log("Using alt servers: " + JSON.stringify(servers));
    } else {
        servers = socketConfig.servers;
    }

    var chosenServer = null;
    servers.forEach(function (server) {
        if (chosenServer === null) {
            chosenServer = server;
        } else if (server.secure && !chosenServer.secure) {
            chosenServer = server;
        } else if (!server.ipv6Only && chosenServer.ipv6Only) {
            chosenServer = server;
        }
    });

    console.log("Connecting to " + JSON.stringify(chosenServer));

    if (chosenServer === null) {
        makeAlert("Error",
                "Socket.io configuration was unable to find a suitable server",
                "alert-danger")
            .appendTo($("#announcements"));
    }

    var opts = {
        secure: chosenServer.secure
    };

    window.socket = io(chosenServer.url, opts);
}

var USING_LETS_ENCRYPT = false;

function initSocketIO(socketConfig) {
    function genericConnectionError() {
        var message = "The socket.io library could not be loaded from <code>" +
                      source + "</code>.  Ensure that it is not being blocked " +
                      "by a script blocking extension or firewall and try again.";
        makeAlert("Error", message, "alert-danger")
            .appendTo($("#announcements"));
        Callbacks.disconnect();
    }

    if (typeof io === "undefined") {
        var script = document.getElementById("socketio-js");
        var source = "unknown";
        if (script) {
            source = script.src;
        }

        if (/^https/.test(source) && location.protocol === "http:"
                && USING_LETS_ENCRYPT) {
            checkLetsEncrypt(socketConfig, genericConnectionError);
            return;
        }

        genericConnectionError();
        return;
    }

    ioServerConnect(socketConfig);
    setupCallbacks();
}

function checkLetsEncrypt(socketConfig, nonLetsEncryptError) {
    var servers = socketConfig.servers.filter(function (server) {
        return !server.secure && !server.ipv6Only
    });

    if (servers.length === 0) {
        nonLetsEncryptError();
        return;
    }

    $.ajax({
        url: servers[0].url + "/socket.io/socket.io.js",
        dataType: "script",
        timeout: 10000
    }).done(function () {
        var message = "Your browser cannot connect securely because it does " +
                      "not support the newer Let's Encrypt certificate " +
                      "authority.  Click below to acknowledge and continue " +
                      "connecting over an unencrypted connection.  See " +
                      "<a href=\"https://community.letsencrypt.org/t/which-browsers-and-operating-systems-support-lets-encrypt/4394\" target=\"_blank\">here</a> " +
                      "for more details.";
        var connectionAlert = makeAlert("Error", message, "alert-danger")
            .appendTo($("#announcements"));

        var button = document.createElement("button");
        button.className = "btn btn-default";
        button.textContent = "Connect Anyways";

        var alertBox = connectionAlert.find(".alert")[0];
        alertBox.appendChild(document.createElement("hr"));
        alertBox.appendChild(button);

        button.onclick = function connectAnyways() {
            ioServerConnect({
                servers: servers
            });
            setupCallbacks();
        };
    }).error(function () {
        nonLetsEncryptError();
    });
}

(function () {
    $.getJSON("/socketconfig/" + CHANNEL.name + ".json")
        .done(function (socketConfig) {
            initSocketIO(socketConfig);
        }).fail(function () {
            makeAlert("Error", "Failed to retrieve socket.io configuration.  " +
                               "Please try again in a few minutes.",
                    "alert-danger")
                .appendTo($("#announcements"));
            Callbacks.disconnect();
        });
})();





// -------------------------

/* window focus/blur */
$(window).focus(function() {
    FOCUSED = true;
    clearInterval(TITLE_BLINK);
    TITLE_BLINK = false;
    document.title = PAGETITLE;
}).blur(function() {
    FOCUSED = false;
});

/* chatbox */

$("#modflair").click(function () {
    var m = $("#modflair");
    if (m.hasClass("label-success")) {
        USEROPTS.modhat = false;
        m.removeClass("label-success");
        if (SUPERADMIN) {
            USEROPTS.adminhat = true;
            m.addClass("label-danger");
        } else {
            m.addClass("label-default");
        }
    } else if (m.hasClass("label-danger")) {
        USEROPTS.adminhat = false;
        m.removeClass("label-danger")
            .addClass("label-default");
    } else {
        USEROPTS.modhat = true;
        m.removeClass("label-default")
            .addClass("label-success");
    }
    $("#us-modflair").prop("checked", USEROPTS.modhat);
    setOpt('modhat', USEROPTS.modhat);
});

$("#usercount").mouseenter(function (ev) {
    var breakdown = calcUserBreakdown();
    // re-using profile-box class for convenience
    var popup = $("<div/>")
        .addClass("profile-box")
        .css("top", (ev.clientY + 5) + "px")
        .css("left", (ev.clientX) + "px")
        .appendTo($("#usercount"));

    var contents = "";
    for(var key in breakdown) {
        contents += "<strong>" + key + ":&nbsp;</strong>" + breakdown[key];
        contents += "<br>"
    }

    popup.html(contents);
});

$("#usercount").mousemove(function (ev) {
    var popup = $("#usercount").find(".profile-box");
    if(popup.length == 0)
        return;

    popup.css("top", (ev.clientY + 5) + "px");
    popup.css("left", (ev.clientX) + "px");
});

$("#usercount").mouseleave(function () {
    $("#usercount").find(".profile-box").remove();
});

$("#messagebuffer").scroll(function (ev) {
    if (IGNORE_SCROLL_EVENT) {
        // Skip event, this was triggered by scrollChat() and not by a user action.
        // Reset for next event.
        IGNORE_SCROLL_EVENT = false;
        return;
    }

    var m = $("#messagebuffer");
    var lastChildHeight = 0;
    var messages = m.children();
    if (messages.length > 0) {
        lastChildHeight = messages[messages.length - 1].clientHeight || 0;
    }

    var isCaughtUp = m.height() + m.scrollTop() >= m.prop("scrollHeight") - lastChildHeight;
    if (isCaughtUp) {
        SCROLLCHAT = true;
        $("#newmessages-indicator").remove();
    } else {
        SCROLLCHAT = false;
    }
});

$("#guestname").keydown(function (ev) {
    if (ev.keyCode === 13) {
        socket.emit("login", {
            name: $("#guestname").val()
        });
    }
});

function chatTabComplete() {
    var words = $("#chatline").val().split(" ");
    var current = words[words.length - 1].toLowerCase();
    if (!current.match(/^[\w-]{1,20}$/)) {
        return;
    }

    var __slice = Array.prototype.slice;
    var usersWithCap = __slice.call($("#userlist").children()).map(function (elem) {
        return elem.children[1].innerHTML;
    });
    var users = __slice.call(usersWithCap).map(function (user) {
        return user.toLowerCase();
    }).filter(function (name) {
        return name.indexOf(current) === 0;
    });

    // users now contains a list of names that start with current word

    if (users.length === 0) {
        return;
    }

    // trim possible names to the shortest possible completion
    var min = Math.min.apply(Math, users.map(function (name) {
        return name.length;
    }));
    users = users.map(function (name) {
        return name.substring(0, min);
    });

    // continually trim off letters until all prefixes are the same
    var changed = true;
    var iter = 21;
    while (changed) {
        changed = false;
        var first = users[0];
        for (var i = 1; i < users.length; i++) {
            if (users[i] !== first) {
                changed = true;
                break;
            }
        }

        if (changed) {
            users = users.map(function (name) {
                return name.substring(0, name.length - 1);
            });
        }

        // In the event something above doesn't generate a break condition, limit
        // the maximum number of repetitions
        if (--iter < 0) {
            break;
        }
    }

    current = users[0].substring(0, min);
    for (var i = 0; i < usersWithCap.length; i++) {
        if (usersWithCap[i].toLowerCase() === current) {
            current = usersWithCap[i];
            break;
        }
    }

    if (users.length === 1) {
        if (words.length === 1) {
            current += ":";
        }
        current += " ";
    }
    words[words.length - 1] = current;
    $("#chatline").val(words.join(" "));
}

$("#chatline").keydown(function(ev) {
    // Enter/return
    if(ev.keyCode == 13) {
        if (CHATTHROTTLE) {
            return;
        }
        var msg = $("#chatline").val();
        if(msg.trim()) {
            var meta = {};
            if (USEROPTS.adminhat && CLIENT.rank >= 255) {
                msg = "/a " + msg;
            } else if (USEROPTS.modhat && CLIENT.rank >= Rank.Moderator) {
                meta.modflair = CLIENT.rank;
            }

            // The /m command no longer exists, so emulate it clientside
            if (CLIENT.rank >= 2 && msg.indexOf("/m ") === 0) {
                meta.modflair = CLIENT.rank;
                msg = msg.substring(3);
            }

            socket.emit("chatMsg", {
                msg: msg,
                meta: meta
            });
            CHATHIST.push($("#chatline").val());
            CHATHISTIDX = CHATHIST.length;
            $("#chatline").val("");
        }
        return;
    }
    else if(ev.keyCode == 9) { // Tab completion
        chatTabComplete();
        ev.preventDefault();
        return false;
    }
    else if(ev.keyCode == 38) { // Up arrow (input history)
        if(CHATHISTIDX == CHATHIST.length) {
            CHATHIST.push($("#chatline").val());
        }
        if(CHATHISTIDX > 0) {
            CHATHISTIDX--;
            $("#chatline").val(CHATHIST[CHATHISTIDX]);
        }

        ev.preventDefault();
        return false;
    }
    else if(ev.keyCode == 40) { // Down arrow (input history)
        if(CHATHISTIDX < CHATHIST.length - 1) {
            CHATHISTIDX++;
            $("#chatline").val(CHATHIST[CHATHISTIDX]);
        }

        ev.preventDefault();
        return false;
    }
});

/* poll controls */
//$("#newpollbtn").click(showPollMenu);
$("#newpollbtn").click(function() {
	var msg = $("#guestname").val();
	console.log(msg);
	if (!msg.trim()) return;
	
	socket.emit("chatMsg2", {
        msg: msg,
        meta: {}
    });
    CHATHIST.push($("#guestname").val());
    CHATHISTIDX = CHATHIST.length;
    $("#guestname").val("");
});





/* load channel */

var loc = document.location+"";
var m = loc.match(/\/r\/([a-zA-Z0-9-_]+)/);
if(m) {
    CHANNEL.name = m[1];
    if (CHANNEL.name.indexOf("#") !== -1) {
        CHANNEL.name = CHANNEL.name.substring(0, CHANNEL.name.indexOf("#"));
    }
}

/* channel ranks stuff */
function chanrankSubmit(rank) {
    var name = $("#cs-chanranks-name").val();
    socket.emit("setChannelRank", {
        name: name,
        rank: rank
    });
}
$("#cs-chanranks-mod").click(chanrankSubmit.bind(this, 2));
$("#cs-chanranks-adm").click(chanrankSubmit.bind(this, 3));
$("#cs-chanranks-owner").click(chanrankSubmit.bind(this, 4));

["#showmediaurl", "#showsearch", "#showcustomembed", "#showplaylistmanager"]
    .forEach(function (id) {
    $(id).click(function () {
        var wasActive = $(id).hasClass("active");
        $(".plcontrol-collapse").collapse("hide");
        $("#plcontrol button.active").button("toggle");
        if (!wasActive) {
            $(id).button("toggle");
        }
    });
});
$("#plcontrol button").button();
$("#plcontrol button").button("hide");
$(".plcontrol-collapse").collapse();
$(".plcontrol-collapse").collapse("hide");

$(".cs-checkbox").change(function () {
    var box = $(this);
    var key = box.attr("id").replace("cs-", "");
    var value = box.prop("checked");
    var data = {};
    data[key] = value;
    socket.emit("setOptions", data);
});

$(".cs-textbox").keyup(function () {
    var box = $(this);
    var key = box.attr("id").replace("cs-", "");
    var value = box.val();
    var lastkey = Date.now();
    box.data("lastkey", lastkey);

    setTimeout(function () {
        if (box.data("lastkey") !== lastkey || box.val() !== value) {
            return;
        }

        var data = {};
        if (key.match(/chat_antiflood_(burst|sustained)/)) {
            data = {
                chat_antiflood_params: {
                    burst: $("#cs-chat_antiflood_burst").val(),
                    sustained: $("#cs-chat_antiflood_sustained").val()
                }
            };
        } else {
            data[key] = value;
        }
        socket.emit("setOptions", data);
    }, 1000);
});

$("#cs-chanlog-refresh").click(function () {
    socket.emit("readChanLog");
});

$("#cs-chanlog-filter").change(filterChannelLog);

$("#cs-motdsubmit").click(function () {
    socket.emit("setMotd", {
        motd: $("#cs-motdtext").val()
    });
});

$("#cs-csssubmit").click(function () {
    socket.emit("setChannelCSS", {
        css: $("#cs-csstext").val()
    });
});

$("#cs-jssubmit").click(function () {
    socket.emit("setChannelJS", {
        js: $("#cs-jstext").val()
    });
});

$("#cs-chatfilters-newsubmit").click(function () {
    var name = $("#cs-chatfilters-newname").val();
    var regex = $("#cs-chatfilters-newregex").val();
    var flags = $("#cs-chatfilters-newflags").val();
    var replace = $("#cs-chatfilters-newreplace").val();
    var entcheck = checkEntitiesInStr(regex);
    if (entcheck) {
        alert("Warning: " + entcheck.src + " will be replaced by " +
              entcheck.replace + " in the message preprocessor.  This " +
              "regular expression may not match what you intended it to " +
              "match.");
    }

    socket.emit("addFilter", {
        name: name,
        source: regex,
        flags: flags,
        replace: replace,
        active: true
    });

    socket.once("addFilterSuccess", function () {
        $("#cs-chatfilters-newname").val("");
        $("#cs-chatfilters-newregex").val("");
        $("#cs-chatfilters-newflags").val("");
        $("#cs-chatfilters-newreplace").val("");
    });
});

$("#cs-emotes-newsubmit").click(function () {
    var name = $("#cs-emotes-newname").val();
    var image = $("#cs-emotes-newimage").val();

    socket.emit("updateEmote", {
        name: name,
        image: image,
    });

    $("#cs-emotes-newname").val("");
    $("#cs-emotes-newimage").val("");
});

$("#cs-chatfilters-export").click(function () {
    var callback = function (data) {
        socket.listeners("chatFilters").splice(
            socket.listeners("chatFilters").indexOf(callback)
        );

        $("#cs-chatfilters-exporttext").val(JSON.stringify(data));
    };

    socket.on("chatFilters", callback);
    socket.emit("requestChatFilters");
});

$("#cs-chatfilters-import").click(function () {
    var text = $("#cs-chatfilters-exporttext").val();
    var choose = confirm("Вы собираетесь импортировать фильтры из текстового поля, находящегося под кнопкой импорта. Если поле пусто, данное действие очистит все ваши фильтры. Вы уверены что хотите продолжить?");
    if (!choose) {
        return;
    }

    if (text.trim() === "") {
        text = "[]";
    }

    var data;
    try {
        data = JSON.parse(text);
    } catch (e) {
        alert("Неправильные данные импорта: " + e);
        return;
    }

    socket.emit("importFilters", data);
});

$("#cs-emotes-export").click(function () {
    var em = CHANNEL.emotes.map(function (f) {
        return {
            name: f.name,
            image: f.image
        };
    });
    $("#cs-emotes-exporttext").val(JSON.stringify(em));
});

$("#cs-emotes-import").click(function () {
    var text = $("#cs-emotes-exporttext").val();
    var choose = confirm("Вы собираетесь импортировать смайлы из текстового поля, находящегося под кнопкой импорта. Если поле пусто, данное действие очистит все ваши смайлы. Вы уверены что хотите продолжить?");
    if (!choose) {
        return;
    }

    if (text.trim() === "") {
        text = "[]";
    }

    var data;
    try {
        data = JSON.parse(text);
    } catch (e) {
        alert("Неправильные данные импорта: " + e);
        return;
    }

    socket.emit("importEmotes", data);
});

var toggleUserlist = function () {
    var direction = !USEROPTS.layout.match(/synchtube/) ? "glyphicon-chevron-right" : "glyphicon-chevron-left"
    if ($("#userlist").css("display") === "none") {
        $("#userlist").show();
        $("#userlisttoggle").removeClass(direction).addClass("glyphicon-chevron-down");
    } else {
        $("#userlist").hide();
        $("#userlisttoggle").removeClass("glyphicon-chevron-down").addClass(direction);
    }
    scrollChat();
};

$("#usercount").click(toggleUserlist);
$("#userlisttoggle").click(toggleUserlist);

$(".add-temp").change(function () {
    $(".add-temp").prop("checked", $(this).prop("checked"));
});

/*
 * Fixes #417 which is caused by changes in Bootstrap 3.3.0
 * (see twbs/bootstrap#15136)
 *
 * Whenever the active tab in channel options is changed,
 * the modal must be updated so that the backdrop is resized
 * appropriately.
 */
$("#channeloptions li > a[data-toggle='tab']").on("shown.bs.tab", function () {
    $("#channeloptions").data("bs.modal").handleUpdate();
});

applyOpts();

(function () {
    var embed = document.querySelector("#videowrap .embed-responsive");
    if (!embed) {
        return;
    }

    if (typeof window.MutationObserver === "function") {
        var mr = new MutationObserver(function (records) {
            records.forEach(function (record) {
                if (record.type !== "childList") return;
                if (!record.addedNodes || record.addedNodes.length === 0) return;

                var elem = record.addedNodes[0];
                if (elem.id === "ytapiplayer") handleVideoResize();
            });
        });

        mr.observe(embed, { childList: true });
    } else {
        /*
         * DOMNodeInserted is deprecated.  This code is here only as a fallback
         * for browsers that do not support MutationObserver
         */
        embed.addEventListener("DOMNodeInserted", function (ev) {
            if (ev.target.id === "ytapiplayer") handleVideoResize();
        });
    }
})();

var EMOTELISTMODAL = $("#emotelist");
EMOTELISTMODAL.on("hidden.bs.modal", unhidePlayer);
$("#emotelistbtn").click(function () {
    EMOTELISTMODAL.modal();
});

EMOTELISTMODAL.find(".emotelist-alphabetical").change(function () {
    USEROPTS.emotelist_sort = this.checked;
    setOpt("emotelist_sort", USEROPTS.emotelist_sort);
});
EMOTELISTMODAL.find(".emotelist-alphabetical").prop("checked", USEROPTS.emotelist_sort);


$("#fullscreenbtn").click(function () {
    var elem = document.querySelector("#videowrap .embed-responsive");
    // this shit is why frontend web development sucks
    var fn = elem.requestFullscreen ||
        elem.mozRequestFullScreen || // Mozilla has to be different and use a capital 'S'
        elem.webkitRequestFullscreen ||
        elem.msRequestFullscreen;

    if (fn) {
        fn.call(elem);
    }
});

function handleCSSJSTooLarge(selector) {
    if (this.value.length > 20000) {
        var warning = $(selector);
        if (warning.length > 0) {
            return;
        }

        warning = makeAlert("Превышен максимальный размер", "Внутренние CSS и JavaScript файлы " +
                "должны иметь менее 20,000 символов. Если вам нужно больше места, " +
                "используйте внешние CSS или JavaScript файлы.", "alert-danger")
                .attr("id", selector.replace(/#/, ""));
        warning.insertBefore(this);
    } else {
        $(selector).remove();
    }
}

$("#cs-csstext").bind("input", handleCSSJSTooLarge.bind($("#cs-csstext")[0],
        "#cs-csstext-too-big"));
$("#cs-jstext").bind("input", handleCSSJSTooLarge.bind($("#cs-jstext")[0],
        "#cs-jstext-too-big"));
