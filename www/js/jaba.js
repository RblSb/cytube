$('#insert_template')
    .click(function() {
        $('#mediaurl').val('http://projects.annimon.com/1.mp4');
    });

var spl;
(function() {
  spl = Split(['#chatwrap', '#videowrap'], {
    sizes: [40, 60],
    onDragEnd: function() {
      window.dispatchEvent(new Event('resize'));
    }
  });
  window.dispatchEvent(new Event('resize'));
})();

var isExtended = false;

$('#extendplayer')
    .click(function() {
        console.log(isExtended);
        if (isExtended) {
            spl.setSizes([40, 60]);
            $('#userlist').width(120);
        } else {
            spl.setSizes([20, 80]);
            $('#userlist').width(80);
        }
        isExtended = !isExtended;
        /*if ($('#chatwrap').attr('class') == 'col-lg-5 col-md-5') {
          $('#chatwrap').attr('class', 'col-lg-3 col-md-3');
          $('#videowrap').attr('class', 'col-lg-9 col-md-9');
          $('#userlist').width(80);
        } else {
          $('#chatwrap').attr('class', 'col-lg-5 col-md-5');
          $('#videowrap').attr('class', 'col-lg-7 col-md-7');
          $('#userlist').width(120);
        }*/
        
        window.dispatchEvent(new Event('resize'));
      });

doc = document;
getChatLine = doc.getElementById('chatline');
getLeftCtrls = doc.getElementById('leftcontrols');
 
var changeBg = function() {
    var images = [
        "http://i.imgur.com/EuHkJo9.jpg", "http://i.imgur.com/NHwTrHD.jpg", "http://i.imgur.com/k9lnetG.jpg",
        "http://i.imgur.com/mQIwwtW.jpg", "http://i.imgur.com/ee3fKEh.jpg", "http://i.imgur.com/t9frpjR.jpg",
        "http://i.imgur.com/w7mxX7M.jpg", "http://i.imgur.com/zCX265I.jpg", "http://i.imgur.com/7Nh4gxZ.jpg",
        "http://i.imgur.com/TGE2xWa.jpg", "http://i.imgur.com/64pdGiw.jpg", "http://i.imgur.com/e1iR6pT.jpg",
		"http://i.imgur.com/L1fB70i.jpg", "http://i.imgur.com/GRHpE4h.jpg", "http://i.imgur.com/vPKNttC.jpg",
		"http://i.imgur.com/LKUlNEo.jpg", "http://i.imgur.com/t9ZrAi9.jpg", "http://i.imgur.com/wvE1OBz.jpg", 
		"http://i.imgur.com/aWulWFb.jpg", "http://i.imgur.com/rtQig2o.jpg"
    ];
    var a = Math.round(Math.random() * (images.length-1));
    $('body').css('background','#000 url(' + images[a] +') no-repeat center fixed');
};
$(document).ready(changeBg);
$('#changebgbtn').show();
$('#changebgbtn').click(changeBg);


//======Nano_lib v0.12.28
//======Author: JAlB (2014)
//======License: Beerware

function $id(ID){
if(ID == '@body'){
return document.body;
}else{
return document.getElementById(ID);
}
}

function $Selector(SELECTOR){
return document.querySelectorAll(SELECTOR);
}

function $Class(CLASS) {
return document.getElementsByClassName(CLASS);
};

function $Random(MIN, MAX) {
    return Math.floor(Math.random() * (MAX - MIN + 1)) + MIN;
};

//Добавление элементов и удаление элементов.
function $Create(TYPE, ID, CLASS, OBJTYPE){
if ($id(ID) == null){
 var result = document.createElement(TYPE);
 result.id = ID;
 result.className = CLASS;
 if (OBJTYPE != null) {
 	result.type = OBJTYPE;
 }
 return result;
 } else {
console.error('$Create: Элемент '+ID+' уже существует');
return null;
}
}

function $Add(TYPE, ID, CLASS, ToID){
if($id(ToID) != null){
 result = $Create(TYPE, ID, CLASS);
 if (result != null){
  $id(ToID).appendChild(result);
 } else {
  console.warn('$Add: Элемент '+ID+' не создан.');
 }
 return result;
} else {
 console.error('$Add: Элемент '+ToID+' не найден.');
 }
}

function $RemoveID(ID){
var element = $id(ID);
element.parentNode.removeChild(element);
}

function $Remove(OBJ){
OBJ.parentNode.removeChild(OBJ);
}
//Конец

//Локальное хранилище
function $LSGet(PROPERTY){
return window.localStorage.getItem(PROPERTY);
}

function $LSSet(PROPERTY, VALUE){
window.localStorage.setItem(PROPERTY, VALUE);
}

//JSON
function $JsonEncode(OBJ){
return JSON.stringify(OBJ);
}

function $JsonDecode(STR){
return JSON.parse(STR);
}

//Конец Nano_lib


//======Synchtube API v 0.15.724
//======Author: JAlB (2014)
//======License: Beerware


// INTERFACE FUNCTIONS
/***** add_button *****/
$0BUTTONS = [];
function API_ADDBUTTON(ID, CAPTION, ONCLICK, POSTFIX){
	if ($id(ID) == null) {
		$0BUTTONS[$0BUTTONS.length] = $Add('button', ID, 'btn btn-sm btn-default', 'leftcontrols');
		}
		var BTN = $id(ID)
		BTN.innerHTML = CAPTION;
		BTN.onclick = ONCLICK;
		if (POSTFIX != null) {
			POSTFIX(BTN);
		}
}
/***** END add_button *****/
/***** Add well frame *****/
$0WELLS = [];
function API_ADDWELL(ID, POSTFIX) {
	if ($id(ID) == null) {
		$0WELLS[$0WELLS.length] = $Add('div', ID, 'well', 'pollwrap');
		}
		var WELL = $id(ID)
		if (POSTFIX != null) {
			POSTFIX(WELL);
		}
}
/***** END add well frame *****/
/***** imgur ajax *****/
var $0IMGURONFILESET = [];
function API_IMGURONFILESET(FUNCTION) {
	$0IMGURONFILESET[$0IMGURONFILESET.length] = FUNCTION;
}
var $0IMGURONLOAD = [];
function API_IMGURONLOAD(FUNCTION) {
	$0IMGURONLOAD[$0IMGURONLOAD.length] = FUNCTION;
}
var $0IMGURONPROGRESS = [];
function API_IMGURONPROGRESS(FUNCTION) {
	$0IMGURONPROGRESS[$0IMGURONPROGRESS.length] = FUNCTION;
}
function API_IMGURUPLOAD(file) {
	if (!file || !file.type.match(/image.*/)) return;
	var DATA = new FormData();
	DATA.append("image", file);
	DATA.append("key", "6528448c258cff474ca9701c5bab6927");
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "https://api.imgur.com/3/image");
	xhr.setRequestHeader('Authorization', 'Client-ID a11c2b9fbdd104a');
	xhr.onload = function() {
		result = JSON.parse(xhr.responseText).data.link;
		for (var i = 0; i < $0IMGURONLOAD.length; i++) {
			$0IMGURONLOAD[i](result);
		}
	}
	xhr.onerror = function(){
		alert('Во время загрузки файла произошла ошибка: ' + xhr.status);
	};
	xhr.upload.onprogress = function(EVENT){
		for (var i = 0; i < $0IMGURONPROGRESS.length; i++) {
			$0IMGURONPROGRESS[i](EVENT);
		}
	};
	xhr.send(DATA);
	$id('imgur_fileinput').value = '';
 }
/***** END imgur ajax *****/
/***** imgur upload *****/
function API_CREATEIMGURUPLOADWELL() {
	API_ADDWELL('imgur_upload_well', function (OBJ) {OBJ.style.display = 'none';});
	FILEINPUT = $Create('input', 'imgur_fileinput', '', 'file');
	FILEINPUT.style.display = 'none';
	$id('imgur_upload_well').appendChild(FILEINPUT);
	window.ondragover = function(e) {e.preventDefault()}
	window.ondrop = function(e) {
		e.preventDefault();
		if(e.dataTransfer.files[0].type.match(/image.*/)){
			for (var i = 0; i < $0IMGURONFILESET.length; i++) {
				$0IMGURONFILESET[i]();
			}
			API_IMGURUPLOAD(e.dataTransfer.files[0]);
		} else {
			alert('Можно загружать только изображения.');
			this.value = '';
		}
	}
	IMGUR_FILEINPUTBTN = $Add('button', 'imgur_fileinputbtn', 'btn btn-sm btn-default', 'imgur_upload_well');
	IMGUR_FILEINPUTBTN.innerHTML = 'Загрузить изображение';
	IMGUR_FILEINPUTBTN.onclick = function () {FILEINPUT.click()};
	FILEINPUT.onchange = function(){
		if(this.files[0].type.match(/image.*/)){
			for (var i = 0; i < $0IMGURONFILESET.length; i++) {
				$0IMGURONFILESET[i]();
			}
			API_IMGURUPLOAD(this.files[0]);
		} else {
			alert('Можно загружать только изображения.');
			this.value = '';
		}
	}
}
/***** END imgur upload *****/
// Загрузка изображений
API_CREATEIMGURUPLOADWELL();
API_IMGURONLOAD(function (result) {
$id('chatline').value += result;
$id('image-btn').onclick = image_btn_onclick;
$id('image-btn').innerHTML = 'Загрузить картинку';
$id('image-btn').style.background = '';
});

API_IMGURONFILESET(function(){
	$id('image-btn').onclick = undefined;
	$id('image-btn').innerHTML = 'Загрузка...';
});

API_IMGURONPROGRESS(function(EVENT){
	if (EVENT.lengthComputable){
		var PROGRESS = Math.floor((EVENT.loaded / EVENT.total) * 100);
		$id('image-btn').innerHTML = 'Загружено ' + PROGRESS + '%';
		var red = Math.floor(255 - (PROGRESS * 2.55));
		var green = Math.floor(PROGRESS * 2.55);
		$id('image-btn').style.background = 'rgb('+red+','+green+',0)';
	}
});
function image_btn_onclick() {
	FILEINPUT.click();
}
API_ADDBUTTON('image-btn', 'Загрузить картинку', image_btn_onclick);
// Обработка сообщений перед оправкой
/*API_PREFIXBEFORESEND(function(OBJ){
	var msg = OBJ.value;
	msg = msg.replace(/http:\/\/cdn\.syn-ch\.com\/thumb\/.*\/.*\/.*\/(.*?((\.jpg)|(\.jpeg)|(\.png)|(\.gif)))(\s|$|\/)/g, '!!//i.imgur.com/$1');
	msg = msg.replace(/http:\/\/cdn\.syn-ch\.com\/src\/.*\/.*\/.*\/(.*?((\.jpg)|(\.jpeg)|(\.png)|(\.gif)))(\s|$|\/)/g, '!!//i.imgur.com/$1');
	msg = msg.replace(/http:\/\/i.imgur.com\/(.*?)(\s|$)/g, '!!//i.imgur.com/$1');
	msg = msg.replace(/http:\/\/(.*?((\.jpg)|(\.jpeg)|(\.png)|(\.gif)))(\s|$)/g, '!!//i.imgur.com/$1');
	msg = msg.replace(/https:\/\/(.*?((\.jpg)|(\.jpeg)|(\.png)|(\.gif)))(\s|$)/g, '!!//i.imgur.com/$1');
	if(msg != OBJ.value){OBJ.value = msg;}
});
// Обработка новых сообщений.
API_PREFIXMESSAGE(function(data, last){
	console.log(data);
	data.msg = data.msg.replace(/\$i1(.*?)i1\$/g, '<img class="chat-image" src="http://$1">');
	data.msg = data.msg.replace(/\$i2(.*?)i2\$/g, '<img class="chat-image" src="https://$1">');
	data.msg = data.msg.replace(/\$ii(.*?)ii\$/g, '<img class="chat-image" src="http://i.imgur.com/$1">');
	data.msg = data.msg.replace(/\$is(.{3}?)(.{2}?)(.{2}?)(.*?)is\$/g, '<img class="chat-image" src="http://cdn.syn-ch.com/thumb/$1/$2/$3/$1$2$3$4">');
	data.msg = data.msg.replace(/\$iS(.{3}?)(.{2}?)(.{2}?)(.*?)iS\$/g, '<img class="chat-image" src="http://cdn.syn-ch.com/src/$1/$2/$3/$1$2$3$4">');
});
// инициализация смотрелки картинок.
API_IMAGEVIEWERINIT();
// Обработка клика по классу объектов.
API_CLASSCLICKEVENTADD(function(TARGET, CLS){
	if(CLS == 'chat-image'){
		API_IMAGESHOW(TARGET.target.src, TARGET.target.naturalHeight, TARGET.target.naturalWidth);
	}
});*/
